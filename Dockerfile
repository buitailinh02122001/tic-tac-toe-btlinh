FROM node:18-alpine as production

WORKDIR /backend/caro

COPY package*.json ./

RUN npm install 

COPY . .

EXPOSE 8881

RUN npm run build

CMD ["npm", "run", "local-tas"]
