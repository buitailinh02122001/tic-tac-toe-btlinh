const WebSocket = require('ws');
const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const ws = new WebSocket('ws://0.0.0.0:8881');
// docker-compose -p tic-toc-toe up -d
// Add a connect listener 
ws.on('open', async function open() {
  console.log('Connected to server');

  rl.on('line', (input) => {
    // Gửi dữ liệu nhập từ terminal đến server
    testSend(input);

  });
});

ws.on('message', async(response) => {
  // Nhận dữ liệu từ server và hiển thị trên terminal
  console.log(`Received message: `);
  const [error, message] = await catchAsync(decodeMessage(response, './src/network/grpc/caro.proto', 'caro.Response'));
  testReceived(message);
});

ws.on('close', () =>{
  console.log('Server websotket disconnect ....')
  rl.close();
})

const protobuf = require("protobufjs");

const catchAsync = async (promise) => {
    return promise.then(data => [null, data])
      .catch(err => [err]);
  }


  const encodeMessage = async (payload, filePath, packageType) => {
    try {
      const root = await protobuf.load(filePath);
      const testMessage = root.lookupType(packageType);
      const message = testMessage.create(payload);
      return testMessage.encode(message).finish();
    }
    catch (error) {
      console.log(`[encodeMessage] issue: ${error}`);
      throw new ApiError(httpStatus.NOT_ACCEPTABLE, 'Can not encrypt message');
    }
  } 
  
  const decodeMessage = async (buffer, filePath, packageType) => {
    try {
      const root = await protobuf.load(filePath);
      const testMessage = root.lookupType(packageType);
      testMessage.verify(buffer);
      const message = testMessage.decode(buffer);
      return testMessage.toObject(message);
    }
    catch (error) {
      console.log(`[decodeMessage] issue: ${error}`);
      throw new ApiError(httpStatus.BAD_REQUEST, 'Can not decode message');
    }
  }


  async function testSend(data) {
    const { header, payload} = JSON.parse(data||{});

    let error, payloadEn;
    switch(header){
      case '1':
        {
          // {"header": "1"}
          const payloadData = {
              type: 1,
              move: null,
          }
          const [errorDataEn, payloadDataEn] = await catchAsync( encodeMessage(payloadData,'./src/network/grpc/caro.proto', 'caro.Request'));

          const payload1 = {
                header: 10,
                data: payloadDataEn
         };

          [error, payloadEn] = await catchAsync(encodeMessage(payload1, './src/network/grpc/caro.proto', 'caro.PackageData'));
        }
        break
      case '2': 
      {

        // {"header": "2", "payload": {"x":1, "y":1}}
        const { x, y } = payload;
        const payloadData = {
          type: 2,
          move: {
            row: x,
            col: y,
          }
        }
      const [errorDataEn, payloadDataEn] = await catchAsync( encodeMessage(payloadData,'./src/network/grpc/caro.proto', 'caro.Request'));

      const payload1 = {
            header: 10,
            data: payloadDataEn
     };

      [error, payloadEn] = await catchAsync(encodeMessage(payload1, './src/network/grpc/caro.proto', 'caro.PackageData'));
    }
      break

      case '3': 
      {
        // {"header": "3"}
        const payloadData = {
          type: 4,
        }
      const [errorDataEn, payloadDataEn] = await catchAsync( encodeMessage(payloadData,'./src/network/grpc/caro.proto', 'caro.Request'));

      const payload1 = {
            header: 11,
            data: payloadDataEn
     };

      [error, payloadEn] = await catchAsync(encodeMessage(payload1, './src/network/grpc/caro.proto', 'caro.PackageData'));
    }
      break

      case '4': 
      {

        // {"header": "4"}
        const payloadData = {
          type: 5,
          move: null,
        }
      const [errorDataEn, payloadDataEn] = await catchAsync( encodeMessage(payloadData,'./src/network/grpc/caro.proto', 'caro.Request'));

      const payload1 = {
            header: 10,
            data: payloadDataEn
     };

      [error, payloadEn] = await catchAsync(encodeMessage(payload1, './src/network/grpc/caro.proto', 'caro.PackageData'));
    }
      break;

      case '5':
        {
          // {"header": "5"}
          const payloadData = {
              userCodeId: 'message',
              mmr: 123,
          }
          const [errorDataEn, payloadDataEn] = await catchAsync( encodeMessage(payloadData,'./src/network/grpc/caro.proto', 'caro.FindingRoom') );

          const payload1 = {
                header: 1,
                data: payloadDataEn
         };

          [error, payloadEn] = await catchAsync(encodeMessage(payload1, './src/network/grpc/caro.proto', 'caro.PackageData'));
        }
        break;
        default:
          {
          console.error(`header not found!`);
          }
          break;
    }
    const result = [];
    function* hexFormatValues(buffer) {
      for (let x of buffer) {
        const hex = x.toString(16)
        yield hex.padStart(2, '0')
      }
    }
   if(payloadEn){
    for (let hex of hexFormatValues(payloadEn)) {
      result.push(hex);
    }
    ws.send(payloadEn);
   }
  }

  function testReceived(message){

    switch (message.type) {
      case 1:
        {
          console.log('@msg :', message.data);
        }
        break;
      case 2:
        {
          const gameStatus = JSON.parse(message.data);
          console.log('|--------------------------------|')
          console.log('gameStatus :', gameStatus.game_over)
          console.log('Board:')
          console.log('-------------')
          gameStatus.board.forEach( row => 
            {
              console.log('| ' + row[0] + ' | ' + row[1] + ' | ' + row[2] + ' |')
              console.log('-------------')
            })
        }
        break;
        case 3:
          {
            const gameStatus = JSON.parse(message.data);
            console.log('|--------------------------------|')
            console.log('gameStatus :', gameStatus.game_over)
            console.log('Board:')
            console.log('-------------')
            gameStatus.board.forEach( row => 
              {
                console.log('| ' + row[0] + ' | ' + row[1] + ' | ' + row[2] + ' |')
                console.log('-------------')
              })
              console.log('YOU:', gameStatus.you)
          }
          break;
      default:
        {
          console.error('abc test');
        }
        break;
    }

  }

  
  