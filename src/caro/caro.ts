import { ROOM_TYPE } from './../utils/constants';
import User from 'general/user';
import UserManager from '../general/userManager';
import CareRoom from './CareRoom';
import Room from '../general/room';
import RoomManager from '../general/roomManager';

class Caro {

    private roomList: RoomManager
    private userManager: UserManager;

    constructor() {
        this.roomList = new RoomManager();
        this.userManager = new UserManager();
    }

    getRoomList() {
        return this.roomList;
    }

    public async joinRoom(roomInfo: any) {
        const { roomGrId, roomId, firstUser, firstUserId, secondUser, typeRoom } = roomInfo;

        //find room
        let room: CareRoom = this.roomList.get(roomId);

        console.log('find room', room);

        if (!room) {
            //create room
            const params = {
                typeRoom: typeRoom === 1 ? ROOM_TYPE.ASYNCHRONOUS : ROOM_TYPE.SYNCHRONOUS,
                id: roomId,
                roomGrId,
            }

            const user: User = firstUser || secondUser;
            room = new CareRoom(roomId, params);
            room.joinRoom2(user);
            this.roomList.add(room);

            //join room
            if (!!firstUser && !!secondUser) {
                console.log('second user1: ', secondUser)
                room.joinRoom(secondUser);
                // room.addPlayer(secondUser);
            }
        } else {
            // room.joinRoom(secondUser);
            room.joinRoom2(secondUser);
        }

        return room;
    }
    public async userDisconnected(user: User) {
        // const u = this.userManager.get(user.getSocket());
        // if (!u) {
        //     console.log(111, 'Bingo.userDisconnected user not found!');
        //     return;
        // }
        // try {
        //     user.getRoom().userLeave(user)
        // } catch (e) { }
        // this.userManager.delete(user.getSocket());
    }

    cleanRoom(room: Room) {
        // room.players.forEach(user => {
        //     this.userManager.delete(user.getSocket());
        // }
        // );
        // this.roomList.removeRoom(room.id());
    }
}

export default Caro;