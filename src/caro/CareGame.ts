import User from '../general/user';
import { GAME_OVER_TYPE, ROOM_TYPE } from './../utils/constants';
import { encodeMessage } from './../utils/helpers';
enum SLOT_TYPE {
    NORMAL = 0,
    SELECTED = 1,
}

enum RESULT {
    WIN = 'WIN',
    LOSE = 'LOSE',
    DRAW = 'DRAW',
}

const number = 3;


class CaroGame {
    private players = Array();
    private selected;
    private board = Array();
    private currentPlayer;
    private gameOverType: GAME_OVER_TYPE;
    private status = 0;
    private turn: string;
    private startTime = null;
    private endTime = null;
    private time = null;
    private Winner = null;
    private typeRoom: ROOM_TYPE;

    constructor(players, board) {
        this.selected = Array();
        this.board = JSON.parse(JSON.stringify(board));
        this.players = players;
        this.currentPlayer = this.players[0];
        this.gameOverType = GAME_OVER_TYPE.NONE;
        this.turn = 'X';
        this.startTime = Date.now();

        if (players.length === 1) {
            this.typeRoom = ROOM_TYPE.ASYNCHRONOUS;
        }


        // this.send()
    }


    public static newBoard() {
        return Array(number).fill(' ').map(() => Array(number).fill(' '));
    }

    startGame = () => {
        console.log('Start game ....')
    }

    /**
     * Description: func process move in the broad  
     * Create by: BTLinh(17/03/2023)
     * Edited by: BTLinh(20/03/2023)
     * @param x: row
     * @param y: column
     * @param player
     * @returns broad
     */
    move = async (x, y, player) => {
        if (x < 0 || x > 2 || y < 0 || y > 2 || this.board[x][y] !== ' ' || player.userCodeId !== this.currentPlayer.userCodeId) {
            return this.board;
        }
        await this.selectedNum(x, y);

        if (this.isGameOver()) {
            console.log('Game Over');
            this.endTime = Date.now();
            this.time = this.endTime - this.startTime;
            await this.send();
            this.gameOverType = GAME_OVER_TYPE.SUCCESS;
            return this.board;
        }
        this.toggleTurn();
        this.currentPlayer = await this.getNextPlayer();
        if (this.players.length === 1) {
            await this.rbMove();
        }
        await this.sendOtherPlayer();
        // console.log('player next', this.currentPlayer.userCodeId, this.turn);
        return this.board;
    }

    getTimePlay() {
        return this.time ? this.time : 0;
    }

    getStatusWin() {
        return this.Winner ? this.Winner : 'error';
    }

    /**
     * Description: func move of server
     * Create by: BTLinh(21/03/2023)
     * Edited by:
     * @returns 
     */
    rbMove = async () => {
        const { x, y } = await this.randomLocaltion();
        await this.selectedNum(x, y);
        if (this.isGameOver()) {
            console.log('Game Over');
            this.endTime = Date.now();
            this.time = this.endTime - this.startTime;
            await this.send();
            this.gameOverType = GAME_OVER_TYPE.SUCCESS;
            return this.board;
        }
        this.toggleTurn();
        this.currentPlayer = await this.getNextPlayer();
        this.sendPVE();
    }

    /**
     * Description: func random localtion x,y
     * Create by: BTLinh(21/03/2023)
     * Edited by:
     * @returns 
     */
    randomLocaltion = async () => {
        let x, y, check = true;
        try {
            while (check) {

                x = this.random();
                y = this.random();
                if (this.board[x][y] === " " && this.currentPlayer === undefined) {
                    check = false;
                }

            };
        } catch (error) {
            console.log(error);
        }
        return { x, y };
    }

    random = () => {
        return Math.floor(Math.random() * 3);
    }

    /**
     * Description: func string item of broad 
     * Create by: BTLinh(17/03/2023)
     * Edited by:
     * @returns 
     */
    toggleTurn() {
        return this.turn = this.turn === 'X' ? 'O' : 'X';
    }
    getNextPlayer() {
        return this.currentPlayer === this.players[0] ? this.players[1] : this.players[0];
    }

    /**
     * Description: check win or full board 
     * Create by: BTLinh(17/03/2023)
     * Edited by:
     * @returns boolean
     */

    isGameOver = () => {
        // console.log(this.checkRows(), this.checkColumns(), this.checkCross(), this.isBoardFull());
        if (this.checkRows() || this.checkColumns() || this.checkCross() || this.isBoardFull()) {

            return true;
        }
        return false;
    }

    /**
     * Description: check rows of the broand
     * Create by: BTLinh(17/03/2023)
     * Edited by:
     * @returns boolean
     */
    checkRows = () => {
        for (let i = 0; i < number; i++) {
            if (this.check(this.board[i][0], this.board[i][1], this.board[i][2])) {
                return true;
            }
        }
        return false;
    }


    /**
     * Description: check column of the board
     * Create by: BTLinh(17/03/2023)
     * Edited by:
     * @returns boolean
     */
    checkColumns = () => {
        for (let i = 0; i < number; i++) {
            if (this.check(this.board[0][i], this.board[1][i], this.board[2][i])) {
                return true;
            }
        }
        return false;
    }

    /**
     * Description: check cross of the board
     * Create by: BTLinh(17/03/2023)
     * Edited by:
     * @returns boolean
     */
    checkCross = () => {
        let result = false;
        if (this.board[0][0] !== ' ' && this.board[0][0] === this.board[1][1] && this.board[0][0] === this.board[2][2]) {
            result = true;
        }

        if (this.board[0][2] !== ' ' && this.board[0][2] === this.board[1][1] && this.board[0][2] === this.board[2][0]) {
            result = true;
        }
        return result;
    }


    check = (x1, x2, x3) => {
        let result = false;
        if (x1 !== ' ' && x1 === x2 && x2 === x3) {
            result = true;
        }
        return result;
    }

    /**
     * Description: function assign value to item of the board 
     * Create by: BTLinh(17/03/2023)
     * Edited by:
     * @param x: row
     * @param y: column
     * @returns 
     */
    selectedNum = (x, y) => {
        this.board[x][y] = this.turn;
        return this.board
    }

    /**
     * Description: function update status game
     * Create by: BTLinh(17/03/2023)
     * Edited by:
     * @param gameOverType
     * @returns 
     */
    endGame = (gameOverType: GAME_OVER_TYPE = 2) => {
        this.gameOverType = gameOverType;
        if (this.isEnd()) return;
        this.status = 1
    }


    isEnd() {
        return this.status = 1;
    }

    /**
     * Description: function check other elements is empty
     * Create by: BTLinh(17/03/2023)
     * Edited by: BTLinh(20/03/2023)
     * @returns 
     */
    isBoardFull = () => {
        let isContinue = true;
        for (const row in this.board) {
            for (const col in this.board[row]) {
                if (this.board[row][col] === ' ') {
                    return isContinue = false;
                }
            }
        }
        if (!isContinue) {
            this.endGame(GAME_OVER_TYPE.SUCCESS);
        }
        return isContinue;
    }

    getState = () => {
        return {
            currentPlayer: this.currentPlayer,
            board: this.board,
            players: this.players,
        };
    }

    /**
     * Description: function send to players when end game
     * Create by: BTLinh(17/03/2023)
     * Edited by: Btlinh(20/03/2023)
     * @returns 
     */
    send = async () => {
        let payload = {};
        if (this.players.length === 1 && this.typeRoom !== ROOM_TYPE.ASYNCHRONOUS) {
            payload['you'] = RESULT.WIN;
            payload['board'] = this.board;
            payload['game_over'] = this.gameOverType
            const packageData = { type: 3, data: JSON.stringify(payload) };
            const buffer = await encodeMessage(packageData, 'src/network/grpc/caro.proto', 'caro.Response');
            this.players[0]?.getSocket().send(buffer)
        }
        else {
            await this.players.forEach(async (player: User) => {
                if (this.checkColumns || this.checkCross || this.checkRows) {
                    payload['you'] = this.currentPlayer === player ? RESULT.WIN : RESULT.LOSE;
                }
                else {
                    payload['you'] = RESULT.DRAW;
                }
                this.Winner = payload['you'];
                console.log('result: ', this.Winner, this.time);
                payload['board'] = this.board;
                payload['game_over'] = this.gameOverType
                const packageData = { type: 3, data: JSON.stringify(payload) };
                const buffer = await encodeMessage(packageData, 'src/network/grpc/caro.proto', 'caro.Response');
                player.getSocket().send(buffer)
            })
        }

    }
    /**
     * Description: function send data to player other
     * Create by: BTLinh(17/03/2023)
     * Edited by:
     * @returns 
     */
    sendOtherPlayer = async () => {
        let payload = {};
        payload['board'] = this.board;
        payload['game_over'] = this.gameOverType

        const packageData = { type: 2, data: JSON.stringify(payload) };
        const buffer = await encodeMessage(packageData, 'src/network/grpc/caro.proto', 'caro.Response');
        this.currentPlayer.getSocket().send(buffer)
    }

    sendPVE = async () => {
        let payload = {};
        await this.players.forEach(async (player: User) => {
            payload['board'] = this.board;
            payload['game_over'] = this.gameOverType
            const packageData = { type: 3, data: JSON.stringify(payload) };
            const buffer = await encodeMessage(packageData, 'src/network/grpc/caro.proto', 'caro.Response');
            player.getSocket().send(buffer)
        })
    }





}

export default CaroGame;