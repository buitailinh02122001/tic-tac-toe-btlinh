import { ROOM_TYPE } from './../utils/constants';
import { encodeMessage } from './../utils/helpers';
import Room from "../general/room";
import CareGame from "./CareGame";
import User from "../general/user";
import CaroGame from "./CareGame";
import { GAME_OVER_TYPE, ROOM_STATUS } from "../utils/constants";
import catchAsync from "../utils/catchAsync";
import { OtherPlayerResult } from 'utils/inteface';

class CareRoom extends Room {
    typeRoom: ROOM_TYPE;
    private listGamePlay: Map<string, CareGame>;
    private otherPlayerResult: OtherPlayerResult | undefined;
    private board = null;

    constructor(id: string, params) {
        super(id)
        this.id = id;
        this.typeRoom = params.typeRoom;
        this.otherPlayerResult = params.otherPlayerResult;
        this.listGamePlay = new Map<string, CaroGame>();
        this.board = CaroGame.newBoard();

    }

    /**
     * Description: function starts the game
     * Create by: BTLinh(17/03/2023)
     * Edited by:
     * @returns 
     */
    startGame = async () => {
        const newGame = new CaroGame(this.players, this.board);
        console.log(this.players)
        await this.players.forEach(async (player) => {

            const packageData = {
                type: 1,
                data: 'Starting player',
            };
            const buffer = await encodeMessage(packageData, './src/network/grpc/caro.proto', 'caro.Response');
            // console.log('buff:', buffer);
            player.getSocket().send(buffer)

        })
        // const userId1 = this.players[0]
        // const userId2 = this.players[1];
        this.listGamePlay.set(this.id, newGame);

        this.players.forEach(async (player) => {
            const data = {
                game_over: ROOM_STATUS.RS_PLAYING,
                winnerId: null,
                board: this.board
            }
            // const buffer1 = await encodeMessage(data, './src/network/grpc/caro.proto', 'caro.GameStatus');
            const packageData = {
                type: 2,
                data: JSON.stringify(data),
            };
            const buffer = await encodeMessage(packageData, './src/network/grpc/caro.proto', 'caro.Response');
            player.getSocket().send(buffer)
        })

    };

    startGameWithPVE = async (user: User) => {
        const players = [user];
        this.board = CaroGame.newBoard()
        const newGame = new CaroGame(players, this.board);
        await players.forEach(async (player) => {

            const packageData = {
                type: 1,
                data: 'Starting player',
            };
            const buffer = await encodeMessage(packageData, './src/network/grpc/caro.proto', 'caro.Response');
            // console.log('buff:', buffer);
            player.getSocket().send(buffer)

        })
        // const userId1 = this.players[0]
        // const userId2 = this.players[1];
        this.listGamePlay.set(this.id, newGame);

        players.forEach(async (player) => {
            const data = {
                game_over: ROOM_STATUS.RS_PLAYING,
                winnerId: null,
                board: this.board
            }
            // const buffer1 = await encodeMessage(data, './src/network/grpc/caro.proto', 'caro.GameStatus');
            const packageData = {
                type: 2,
                data: JSON.stringify(data),
            };
            const buffer = await encodeMessage(packageData, './src/network/grpc/caro.proto', 'caro.Response');
            player.getSocket().send(buffer)
        })

    };

    /**
     * Description: function the action end game
     * Create by: BTLinh(17/03/2023)
     * Edited by:
     * @param user
     * @returns 
     */
    endGameAction = (user: User) => {
        const userId: string = user.getId();
        const gameplay: CaroGame = this.listGamePlay.get(userId);
        gameplay.endGame(GAME_OVER_TYPE.QUIT);
        this.sendResult(user);
    };

    // checkResult = () => {
    //     let size = this.listGamePlay.size;
    //     this.listGamePlay.forEach((game, userId: string) => {
    //         if (game.isEnd()) {
    //             size--;
    //         }
    //     });

    //     if (size == 0) {
    //         this.sendResult();
    //     } else {
    //         setTimeout(() => {
    //             this.checkResult();
    //         }, 1000);
    //     }
    // };

    /**
     * Description: function the action selected location
     * Create by: BTLinh(17/03/2023)
     * Edited by:
     * @param user
     * @param num
     * @returns 
     */
    selectNumber = async (user, num) => {
        try {
            let userId = user.getId();
            const { row, col } = num;
            this.board = await this.listGamePlay.get(this.id).move(row, col, user);
            // this.listGamePlay.get(this.id).isGameOver()
            console.log('Board:', this.board);
            if (this.listGamePlay.get(this.id).isGameOver()) {
                if (this.typeRoom === ROOM_TYPE.ASYNCHRONOUS) {
                    this.sendResult(user);
                }
                setTimeout(() => this.gameEnd(), 5000);
            }
        } catch (error) {
            console.error(error);
        }

    }

    /**
     * Description: function join room for player
     * Create by: BTLinh(17/03/2023)
     * Edited by:
     * @returns 
     */
    joinRoom = async (user: User) => {
        await this.addPlayer(user);
        clearTimeout(this.waitStartTime);
        delete this.waitStartTime;
    }

    joinRoom2(user: User) {
        this.players.push(user);
    }

    /**
     * Description: function wait time 30s for player if not found player other
     * Create by: BTLinh(17/03/2023)
     * Edited by:
     * @returns 
     */
    startWaitTimer() {
        setTimeout(async () => {
            if (this.waitStartTime) {
                console.log(`Room ${this.id} was deleted because no one joined.`);
                const packageData = {
                    type: 1,
                    data: 'Not found user',
                };
                const buffer = await encodeMessage(packageData, './src/network/grpc/caro.proto', 'caro.Response');
                this.players[0].getSocket().send(buffer)
                this.removeRoom();
                global.gameManager.removeCareRoom(this.id);
            }
        }, 30000); // 30 giây
    }

    /**
     * Description: function send a message
     * Create by: BTLinh(17/03/2023)
     * Edited by:
     * @param userId
     * @returns 
     */
    send = (userId) => {
        this.listGamePlay.forEach((game, key: string) => {
            console.log('key: ' + key)
            if (key != userId) {
                game.send()
            }
        })
    }

    /**
     * Description: function end game
     * Create by: BTLinh(17/03/2023)
     * Edited by:
     * @returns 
     */
    gameEnd = async () => {
        await global.gameManager.removeCareRoom(this.id);
        await this.players.forEach(async (player) => {
            player.closeConnection();
        })
    }

    /**
     * Description: function user leave room
     * Create by: BTLinh(17/03/2023)
     * Edited by:
     * @param user
     * @returns 
     */
    userLeave(user: User) {
        try {
            this.listGamePlay.get(this.id).endGame()
        } catch (e) { }
        const index = this.players.indexOf(user);
        if (index > -1) {
            this.players.splice(index, 1);
            user.closeConnection();
            this.listGamePlay.get(this.id).send();
            setTimeout(() => this.gameEnd(), 5000);
        }

    }

    getId() {
        return this.id;
    }

    sendResult = async (user: User) => {
        let otherPlayerResult = this.otherPlayerResult;
        let dataEndUser = undefined;
        let result = undefined;

        this.listGamePlay.forEach((game, userId: string) => {
            let timePlay = game.getTimePlay();
            let statusWin = game.getStatusWin() === 'WIN' ? true : false;
            result = {
                userId: user.getId(),
                timePlay,
                statusWin
            };
            dataEndUser = {
                roomId: this.id,
                result,
                otherPlayerResult: undefined
            };
        });
        if (this.players.length === 1) {
            // otherPlayerResult = result;
            await global.gameManager.updateRoomData(dataEndUser);
            this.otherPlayerResult = dataEndUser.result;
        }
        else {
            dataEndUser.otherPlayerResult = result;
            dataEndUser.result = this.otherPlayerResult;
            console.log(dataEndUser);
            await global.gameManager.updateRoomData(dataEndUser);
        }
        // if (this.players.length === 1) {
        //     console.log("Xoa phong!!");
        // }
    };

}

export default CareRoom;