import 'dotenv/config'
import RedisMatching from "./config/redisMatching";
import RedisClient from "./config/redis";
import MyWebSocket from "./config/websocket"
import GameManager from "./general/gameManager";



const init = async () => {
    await RedisClient.initializer();
    await RedisMatching.initializer();
    const websocket = new MyWebSocket();
    websocket.handlers();
    global.gameManager = new GameManager();

}

init()