import express from 'express';
import helmet from 'helmet';
import cors from 'cors';
import mongoSanitize from 'express-mongo-sanitize';
import compression from 'compression';
// import router
// import passport from 'passport';
import { morganSuccessHandler, morganErrorHandler } from './morgan';
import { errorConverter, errorHandler, notFoundHandler } from '../server/middleware/error';

const app = express();

app.use(morganSuccessHandler);
app.use(morganErrorHandler);

app.use(helmet())
app.use(express.json());
app.use(express.urlencoded({ extended: true }))
app.use(mongoSanitize())
app.use(compression())
app.use(cors())
app.use(errorConverter)
app.use(notFoundHandler)
app.use(errorHandler)

export default app;