import { WebSocket, Server } from "ws";
import router from '../server/routers/websocket'
import { PORT, ENV, REDIS_ADDRESS } from '../utils/utils';
import logger from '../config/logger';
import { authorize } from '../server/middleware/auth.middleware';
import url from 'url';
import app from './express';
import { Socket } from "dgram";
import { Router } from "express";

export default class MyWebSocket {
    private server: Server;
    private serverHttp: any;

    constructor() {
        let WSServer = WebSocket.Server;
        this.serverHttp = require('http').createServer();
        this.server = new WSServer({
            server: this.serverHttp,
        })
        this.serverHttp.on('request', app);
        this.serverHttp.listen(PORT, async () => {
            logger.info(`App listening on ${PORT} (${ENV})`);
        })
    }
    public handlers = () => {
        this.server.on('connection', (socket, req) => {
            logger.info(`Connected ... ${req.socket.remoteAddress}`);

            const websocketRouter = new router(socket);
            websocketRouter.listeners();
        })
    }

}