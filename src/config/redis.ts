import * as redis from 'redis';
import { PORT, REDIS_ADDRESS } from '../utils/utils';
import logger from '../config/logger';
import fs from 'fs';
import path from 'path';
import { promisify } from 'util';
// import { BONUS_GAME_DATA_REDIS_TYPE } from '../utils/constants';
export default class RedisClient {
  private static publisher: any
  public static subscriber: any
  private static redisClient: any;

  static async initializer() {
    try {
      this.publisher = redis.createClient({
        url: REDIS_ADDRESS,
      });
      this.publisher.duplicate();
      await this.publisher.connect();
      logger.info(`Redis - Publisher created ...`);
    } catch (error) {
      logger.error('Redis - Publisher error...', error);
    }
    try {
      this.subscriber = redis.createClient({
        url: REDIS_ADDRESS,
      });
      this.subscriber.duplicate();
      await this.subscriber.connect();
      this.subscriber.subscribe('bonus-game-setting', async (data, error) => {
        if (error) console.log("error::::", error);
        const filePath = path.join(__dirname, '../resource/config/bonusgame.json');
        const dataParse = JSON.parse(JSON.parse(data));
        const { bonusGameReq } = dataParse
      })
      logger.info(`Redis - Subscriber created ...`);
    } catch (error) {
      console.log(error);
      logger.error('Redis - Subscriber error...', error);
    }
    try {
      this.redisClient = redis.createClient({
        url: REDIS_ADDRESS,
      });
      await this.redisClient.connect();
      logger.info(`Redis - Client created ...`);
    } catch (error) {
      logger.error('Redis - Client error...', error);
    }
  }

  static async publish<T>(channel: string, message: T) {
    try {
      logger.info(`Channel: ${channel} message: ${JSON.stringify(message)}`);
      await this.publisher.publish(channel, JSON.stringify(message));
    } catch (error) {
      logger.error('Redis - Publish message error...', error);
    }
  }



  static async get(key) {
    try {
      const data = await this.redisClient.get(key)
      return data ? JSON.parse(data) : null
    } catch (e) {
      logger.error('Redis - get message error...', e);
      return []
    }
  }

  static async set(key, val) {
    try {
      logger.info(`Redis - set ${key} ${val}`);
      await this.redisClient.set(key, JSON.stringify(val))
    } catch (e) {
      logger.error('Redis - set message error...', e);
    }
  }

  static async push(key, value) {
    try {
      console.log(`Redis - push ${key} ${value}`);
      await this.redisClient.set(key, value, function (err, data) {
        if (err) throw err;
      });
      // return data
    } catch (e) {
      logger.error('Redis - rPush message error...', e);
      return null
    }
  }

  static async scan(pattern = "room*", count = 10) {
    try {
      const results = [];
      const iteratorParams = {
        MATCH: pattern,
        COUNT: count
      }
      for await (const key of this.redisClient.scanIterator(iteratorParams)) {
        results.push(key);
      }
      return results;
    } catch (e) {
      logger.error('Redis - scanIterator message error...', e);
      return []
    }
  }

  static async getAll() {
    try {
      const results = [];

      console.log('find ALL')
      this.redisClient.keys('*', function (err, keys) {
        if (err) return console.log(err);
        console.log('keys', keys);
        for (var i = 0, len = keys.length; i < len; i++) {
          console.log(keys[i]);
        }
      })
      return results;
    } catch (e) {
      logger.error('Redis - scanIterator message error...', e);
      return []
    }
  }

  static async del(key) {
    try {
      const res = await this.redisClient.del(key);
      return res
    } catch (e) {
      logger.error('Redis - del message error...', e);
      return null;
    }
  }


}