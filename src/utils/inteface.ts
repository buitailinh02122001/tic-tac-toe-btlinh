import User from "../general/user";

interface OtherPlayerResult {
    userId: string,
    timePlay: number,
    statusWin: boolean,
}

interface ParamsContructorRoom {
    id?: string,
    roomGrId?: string,
    otherPlayerResult?: OtherPlayerResult
}

interface ResFindRoomGroup {
    roomGrId?: string,
    roomId: string,
    status: 'CREATE' | 'JOIN' | null,
    firstUserId?: string,
    secondUserId?: string,
    typeRoom: 1 | 2,
    firstUser?: User,
    secondUser?: User,
}


export {
    ResFindRoomGroup,
    OtherPlayerResult,
    ParamsContructorRoom,
}