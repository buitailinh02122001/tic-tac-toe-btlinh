import protobuf from 'protobufjs';
import ApiError from './APIError';
import logger from '../config/logger';
import httpStatus from 'http-status';

export const toArrayBuffer = (data: string): ArrayBufferLike => {
    var len = data.length;
    var bytes = new Uint8Array(len);
    for (var i = 0; i < len; i++) {
        bytes[i] = data.charCodeAt(i);
    }
    return bytes.buffer;
};

export const encodeMessage = async (payload: any, filePath: string, packageType: string): Promise<Uint8Array | null> => {
    try {
        const root = await protobuf.load(filePath);
        const testMessage = root.lookupType(packageType);
        const message = testMessage.create(payload);
        return testMessage.encode(message).finish();
    }
    catch (error) {
        logger.error(`[encodeMessage] issue: ${error}`);
        throw new ApiError(httpStatus.NOT_ACCEPTABLE, 'Can not encrypt message');
    }
}
export const decodeMessage = async (buffer: any, filePath: string, packageType: string): Promise<{ [k: string]: any } | null> => {
    try {
        const root = await protobuf.load(filePath);
        const testMessage = root.lookupType(packageType);
        testMessage.verify(buffer);
        const message = testMessage.decode(buffer);
        return testMessage.toObject(message);
    }
    catch (error) {
        logger.error(`[decodeMessage] issue: ${error}`);
        throw new ApiError(httpStatus.BAD_REQUEST, 'Can not decode message');
    }
}