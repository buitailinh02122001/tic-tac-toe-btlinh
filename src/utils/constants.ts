export enum GAME_ACTION {
    CREATE_ROOM = "create-room",
    JOIN_ROOM = "join-room",
    END_USER = "end_user",
    END_ROOM = "end-room",
}

export enum GAME_MODE {
    ROUND_ROBIN = 1,
    HEAD_TO_HEAD = 2,
}

export enum GAME_OVER_TYPE {
    TIME_OVER = 0,
    QUIT = 1,
    SUCCESS = 2,
    NONE = 3,
}


export enum ROOM_STATUS {
    RS_NONE = 0,
    RS_WAITING = 1,
    RS_PLAYING = 2,
    RS_STOPPED = 3,
}

export enum PACKAGE_HEADER {
    NONE = 0,
    FINDING_ROOM = 1,
    FINDING_ROOM_RESP = 2,
    CREATE_ROOM = 3,
    CREATE_ROOM_RESP = 4,
    UPDATE_JOIN_ROOM = 5,
    UPDATE_TURN = 6,
    DISCONNECTED = 7,
    PING = 8,
    CARO_GAME = 10,
    USER_DISCONNECTED = 11

}

export const MATCHING_MMR_RANGE = [
    { from: 0, to: 500 },
    { from: 501, to: 1000 },
    { from: 1001, to: 5000 },
    { from: 5001, to: 10000 },
    { from: 10001, to: 50000 },
    { from: 50001, to: null },
];

export enum ROOM_TYPE {
    ASYNCHRONOUS = 1, // bất đồng bộ
    SYNCHRONOUS = 2, // đồng bộ
}