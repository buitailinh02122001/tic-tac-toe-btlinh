export const ENV = process.env.NODE_ENV || 'local';
export const PORT = process.env.PORT || 8881;
export const JWT_SECRET = process.env.JWT_SECRET || '';
export const IS_PRODUCTION = ENV === 'production';
export const REDIS_ADDRESS = process.env.REDIS_ADDRESS || 'redis://';