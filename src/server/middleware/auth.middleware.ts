import jwt from "jsonwebtoken";
import { JWT_SECRET } from "../../utils/utils";


const jwtHandler = (token: string) => {
    try {
        const decoded = jwt.verify(token, JWT_SECRET)
    } catch (error) {
        return false;
    }
};

export const authorize = (token: any) => {
    const decoded = jwtHandler(token);
    if (!decoded) {
        return false;
    }
    return true;
}