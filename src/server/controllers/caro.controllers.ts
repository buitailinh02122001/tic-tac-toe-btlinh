import { encodeMessage } from './../../utils/helpers';
import { ROOM_TYPE } from './../../utils/constants';
import Room from '../../general/room';
import UserManager from '../../general/userManager';
import CareRoom from '../../caro/CareRoom';
import logger from '../../config/logger';
import { WebSocket } from 'ws';
import User from '../../general/user';

export default class CaroController {
    private socket: WebSocket;
    private room: CareRoom;
    private user: User;
    private roomId: any;
    // private room: Room;

    constructor(socket: any) {
        this.socket = socket;
    }


    startGame = async () => {
        let user: User = global.gameManager.userManager.get(this.socket);
        this.user = user
        if (this.user === undefined) {
            const packageData = {
                type: 1,
                data: 'Please find a room before you start',
            };
            const buffer = await encodeMessage(packageData, './src/network/grpc/caro.proto', 'caro.Response');
            this.socket.send(buffer);
        } else {
            this.room = this.user.getRoom() as CareRoom;

            logger.debug('Starting ...');
            if (this.room.typeRoom === ROOM_TYPE.SYNCHRONOUS) {
                this.room.startGame();

            } else {
                this.room.startGameWithPVE(this.user);
            }
        }
    }


    selectNumber = async (move) => {
        await this.room.selectNumber(this.user, move);
    }

    gameEnd = async () => {
        await this.room.gameEnd();
        await this.room.removeRoom();
        console.log("gameEnd");
    }

    userDisConnected = async () => {
        this.room.userLeave(this.user);
    }




}