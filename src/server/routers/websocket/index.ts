import { PACKAGE_HEADER } from './../../../utils/constants';
import { WebSocket } from 'ws';
import { decodeMessage } from '../../../utils/helpers';
import logger from '../../../config/logger';
import catchAsync from '../../../utils/catchAsync';
import User from '../../../general/user';
import CaroRouter from './caro.route';




export default class WSRouter {
    private socket: WebSocket;

    private readonly filePath_tmp = "src/network/grpc/caro.proto";
    private readonly packageType_tnp = "caro.PackageData";
    private caroRouter: CaroRouter;
    // public roomManager: RoomManager;


    constructor(socket: WebSocket) {
        // this.roomManager = new RoomManager();
        this.socket = socket;
        this.caroRouter = new CaroRouter(this.socket);
    }

    public listeners() {
        this.socket.on("message", async (payload) => {
            const [error, message] = await catchAsync(decodeMessage(payload, this.filePath_tmp, this.packageType_tnp));
            // console.log('data123',);
            if (!message || !message?.header) {
                logger.warn(`invalid message / header!`, message);
                console.log(115, message);
                return;
            }

            if (message.header === 7) {
                console.log('disconnect with socket')
                return;
            }
            this.handlers(
                message.header,
                message.data ? Buffer.from(message.data) : [],
                payload
            );

        });


        this.socket.on("close", () => {
            logger.warn(`Client disconnected`);
            this.caroRouter.userDisconnect();
            // global.gameManager.userDisconnected(this.socket);
        });

        this.socket.on("error", () => {
            logger.warn(`Client has error`);
        });
    }

    private handlers = async (header: Number, payload: any, rawData: any) => {
        // console.log(header, payload, rawData);

        switch (header) {

            // case find room
            case PACKAGE_HEADER.FINDING_ROOM:
                {
                    const start = new Date().getTime();
                    let [error, findingRoom] = await catchAsync(
                        decodeMessage(payload, this.filePath_tmp, "caro.FindingRoom")
                    );
                    const user = new User(this.socket);
                    // console.log('user: ', user)

                    user.setData(user.userCodeId, findingRoom.mmr);
                    await global.gameManager.addingQueue(user, findingRoom);
                    findingRoom.userCodeId = user.userCodeId;
                    await user.findPartner(findingRoom, start);
                }
                break;
            // case start game:
            case PACKAGE_HEADER.CARO_GAME:
                {
                    // console.log('test', payload)
                    this.caroRouter.onMessage(payload);

                }
                break;
            case PACKAGE_HEADER.USER_DISCONNECTED:
                {
                    console.log('User disconnected...');
                    this.caroRouter.onMessage(payload);
                }
                break;

            default:
                logger.error(`header not found!`);
                break;
        }
    }
}