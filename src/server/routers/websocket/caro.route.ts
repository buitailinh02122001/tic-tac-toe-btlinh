import logger from '../../../config/logger';
import { decodeMessage } from './../../../utils/helpers';
import CaroController from "../../controllers/caro.controllers";
import catchAsync from "../../../utils/catchAsync";
import { WebSocket } from "ws";
// import RoomManager from '../../../general/roomManager';

enum CARO_MESSAGE_TYPE {
    START_GAME = 1,
    SELECT_NUM = 2,
    END_GAME = 3,
    USER_DIS = 4,
    STAET_GAMEROBOT = 5,
}

export default class CaroRouter {
    private controller: CaroController;
    constructor(socket: WebSocket) {
        this.controller = new CaroController(socket);
    }

    async onMessage(payload: any) {
        const [error, message] = await catchAsync(decodeMessage(payload, 'src/network/grpc/caro.proto', 'caro.Request'));
        // console.log(error, message);
        const type = message.type;
        const move = message.move || 0
        if (!type) {
            logger.warn(`Caro - Unsupported message type`, message);
            return;
        }

        switch (type) {
            case CARO_MESSAGE_TYPE.START_GAME:
                this.controller.startGame();
                break;
            case CARO_MESSAGE_TYPE.SELECT_NUM:
                {
                    // console.log('Game selected', move);
                    this.controller.selectNumber(move);
                }
                break;
            case CARO_MESSAGE_TYPE.END_GAME:
                this.controller.gameEnd();
                break;
            case CARO_MESSAGE_TYPE.USER_DIS:
                this.controller.userDisConnected();
                break;
            // case CARO_MESSAGE_TYPE.STAET_GAMEROBOT:
            //     this.controller.playWithRobot();
            //     break;
            default:
                console.log('Not Exit Game');
                break;
        }
    }

    async userDisconnect() {
        // this.controller.userDisConnected();
    }

    // async test() {
    //     this.controller.startGame();
    // }
}