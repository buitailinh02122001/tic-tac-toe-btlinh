import { ROOM_TYPE } from './../utils/constants';
import { WebSocket } from 'ws';
import PackageProcess from './packageProcess';
import Room from './room';
import { encodeMessage } from '../utils/helpers';
import catchAsync from '../utils/catchAsync';
import { v4 as uuidv4 } from 'uuid';


class User {
    userCodeId: string;
    mmr: number;

    public socket: WebSocket;
    room: Room;

    constructor(socket: WebSocket) {
        this.userCodeId = uuidv4();
        this.socket = socket;
    }


    public getSocket() {
        return this.socket;
    }

    getRoom() {
        return this.room;
    }

    setRoom(room: Room) {
        this.room = room;
    }

    getId() {
        return this.userCodeId;
    }

    closeConnection() {
        console.log(`\n closeConnection`);
        this.socket.close();
    }

    setData(userCodeId: string, mmr: number) {
        this.userCodeId = userCodeId;
        this.mmr = mmr;
    }

    sendData(data: any) {
        return this.socket.send(data);
    }

    public async sendPackage(header: 100, data: any) {
        const packageData = { header, data };
        const buffer = await catchAsync(encodeMessage(packageData, 'src/network/grpc/package.proto', 'caro.PackageData'))
        this.getSocket().send(buffer);
        return buffer;
    }

    async findPartner(searchingData: any, startCheck) {
        const start = startCheck ? startCheck : new Date().getTime();
        let partner = null;
        let isStillExist = false;
        const waitingQueue = global.gameManager.getWaitingQueue(searchingData);

        const indexInQueue = waitingQueue.findIndex((user: User) => searchingData.userCodeId === user.getId());
        console.log("indexInQueue " + this.userCodeId, ' is ', indexInQueue)
        if (indexInQueue > 0) {
            partner = waitingQueue[0];
            const userMatching = [].concat(waitingQueue.splice(indexInQueue, 1)).concat(waitingQueue.splice(0, 1)).reverse();

            console.log("userMatching:::" + this.userCodeId, userMatching)
            global.gameManager.createRoomHeadToHead(searchingData, userMatching, ROOM_TYPE.SYNCHRONOUS)

        }
        else {
            const interval = setInterval(async () => {
                const end = new Date().getTime();
                const timeDiff = end - start;
                const indexInQueue = waitingQueue.findIndex((user: User) => searchingData.userCodeId === user.getId());
                // console.log('acb, ', indexInQueue, searchingData.userCodeId, waitingQueue)
                isStillExist = indexInQueue === -1 ? false : true;
                if (indexInQueue === -1 || timeDiff >= 5000) {
                    console.log("isStillExist::: " + this.userCodeId, isStillExist)
                    if (isStillExist) {
                        // create room asynchronous
                        const userMatching = [].concat(waitingQueue.splice(indexInQueue, 1));
                        const responseFindAsyncRoom = await global.gameManager.searchingForAsyncRoom(userMatching[0], searchingData, start);
                        console.log("responseFindAsyncRoom ::: ", responseFindAsyncRoom)
                        if (!responseFindAsyncRoom) {
                            global.gameManager.createRoomHeadToHead(searchingData, userMatching, ROOM_TYPE.ASYNCHRONOUS);
                        } else {
                            const roomInfo = {
                                ...responseFindAsyncRoom,
                                secondUserId: searchingData.userCodeId,
                                secondUser: userMatching[0],
                            }
                            console.log("roomInfo:::", roomInfo);
                            await global.gameManager.createRoom(roomInfo);
                        }
                        clearInterval(interval);
                    } else {
                        clearInterval(interval);
                    }
                }
            }, 25000);
        }
    }

}

export default User;