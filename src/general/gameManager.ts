import { encodeMessage } from './../utils/helpers';
import { MATCHING_MMR_RANGE, PACKAGE_HEADER } from './../utils/constants';
import UserManager from "./userManager";
import CareRoom from "../caro/CareRoom";
import User from "./user";
import Caro from "../caro/caro";
import logger from "../config/logger";
import RedisMatching from '../config/redisMatching';
import { v4 as uuidv4 } from 'uuid';
import { ResFindRoomGroup } from 'utils/inteface';
import RedisClient from '../config/redis';


class GameManager {
    caro: Caro;
    userManager: UserManager;
    caroRooms: any;
    waitingQueue: any = {};

    public static readonly filePath = 'src/network/grpc/caro.proto';

    constructor() {
        this.caro = new Caro();
        this.userManager = new UserManager();
        this.caroRooms = new Array();

        RedisClient.subscriber.subscribe('update-room', async (data, error) => {
            if (error) console.log("error update-room::::", error);
            await this.updateRoomData(JSON.parse(data));
        });
    }


    public async addingQueue(user: User, msg: any) {
        const { mmr } = msg;
        const { itemMMR } = this.findMMR(mmr);
        const key = `${itemMMR.from}`;
        if (!this.waitingQueue[key]) {
            this.waitingQueue[key] = []
        }
        this.waitingQueue[key].push(user);
        logger.info(`[QUEUE] ${user.getId()}: ${JSON.stringify(this.waitingQueue)}`);
        return null;
    }

    findMMR(mmr) {
        let indexMMR = MATCHING_MMR_RANGE.findIndex((ele) => {
            return ele.from <= mmr && ele.to && ele.to >= mmr;
        });
        if (indexMMR < 0) {
            indexMMR = MATCHING_MMR_RANGE.length - 1;
        }

        return {
            itemMMR: MATCHING_MMR_RANGE[indexMMR],
            indexMMR
        }
    }

    public getWaitingQueue(searchingData: any) {
        if (searchingData) {
            const { mmr } = searchingData;
            const { itemMMR } = this.findMMR(mmr);
            const key = `${itemMMR.from}`;
            if (!this.waitingQueue[key])
                this.waitingQueue[key] = [];
            return this.waitingQueue[key];
        }
        return this.waitingQueue;
    }

    public async searchingForAsyncRoom(user: User, msg?: any, start?: any) {
        let startTime = start ? start : new Date().getTime();
        const { mmr } = msg;
        const { itemMMR } = this.findMMR(mmr);
        const patternScan = `TTL_ROOM_${itemMMR.from}_*`;
        const results = await RedisMatching.scan(patternScan);
        console.log(`[FINDING] scan result `, results)

        for (const roomKey of results) {
            const end = new Date().getTime();
            const timeDiff = end - startTime;
            console.log('timeDiff: ' + timeDiff);
            if (timeDiff >= 30000) {
                return null;
            }

            const exist = await RedisMatching.exists(roomKey);
            console.log(`[FINDING] exist `, exist)
            if (exist) {
                const roomId = roomKey.split('_').pop();
                const roomData = await RedisMatching.hGet(roomId);
                console.log(`[FINDING] roomData`, roomData)
                if (roomData?.firstUserId !== user.getId()) {
                    const numberPlayers = await RedisMatching.increment(`CM_${roomId}`, 1);
                    if (+numberPlayers > 2) {
                        await RedisMatching.del(roomKey);
                    }
                    else {
                        await RedisMatching.del(roomKey);
                        const response = {
                            roomId: roomData.id,
                            status: 'JOIN',
                            firstUserId: roomData.firstUserId || null,
                            typeRoom: 1,
                            round: 1
                        }
                        return response;
                    }
                }
            }
        }
        return null;
    }

    public async createRoomHeadToHead(searchingData, users: Array<User>, typeRoom) {
        const roomId = uuidv4();
        const { mmr } = searchingData;
        const firstUser: User = users[0];
        const roomInfo: ResFindRoomGroup = {
            roomId,
            status: "CREATE",
            firstUserId: firstUser.getId(),
            firstUser,
            typeRoom,
        };

        if (users.length > 1) {
            //add info to roomInfo
            const secondUser: User = users[1];
            roomInfo.secondUserId = secondUser.getId();
            roomInfo.secondUser = secondUser;

        } else {
            const { indexMMR } = this.findMMR(mmr);
            const key = `ROOM_${MATCHING_MMR_RANGE[indexMMR].from}_${roomId}`;
            const setObject = {
                id: roomId,
                createdDate: (new Date()).toISOString(),
                firstUserId: firstUser.getId(),
                firstUser: JSON.stringify(firstUser),
            }

            await RedisMatching.setNX(`CM_${roomId}`, '1');
            await RedisMatching.setEX(`TTL_${key}`, 7 * 24 * 60 * 60, '1');
            await RedisMatching.hSetObject(roomId, setObject);
        }

        //create room
        await this.createRoom(roomInfo);
    }

    public async createRoom(roomInfo) {
        console.log('info room', roomInfo);
        let room = null;
        room = await this.caro.joinRoom(roomInfo);

        const { firstUser, secondUser } = roomInfo;
        // console.log('test', firstUser, secondUser);
        const roomId = room ? room.getId() : null;
        console.log(`id room ${roomId}`);

        if (!!firstUser) {
            firstUser.setRoom(room);

            this.userManager.add(firstUser);

            //send message to user
            const findingRoomResp = {
                roomId: roomId,
                otherPlayerName: secondUser ? secondUser.getId() : null,
                mode: roomInfo.typeRoom
            };

            const buffer = await encodeMessage(findingRoomResp, GameManager.filePath, 'caro.FindingRoom')
            this.sendPackageToUser(firstUser, PACKAGE_HEADER.FINDING_ROOM, JSON.stringify(findingRoomResp));
        }

        if (!!secondUser) {
            secondUser.setRoom(room);
            this.userManager.add(secondUser);
            //send message to user
            const findingRoomResp = {
                roomId,
                otherPlayerName: firstUser ? firstUser.getId() : roomInfo.firstUserId,
                mode: roomInfo.typeRoom,
            };

            const buffer = await encodeMessage(findingRoomResp, GameManager.filePath, 'caro.FindingRoom');
            this.sendPackageToUser(secondUser, PACKAGE_HEADER.FINDING_ROOM, JSON.stringify(findingRoomResp));
        }
    }



    /**
     * Description: function add room to list room 
     * Create by: BTLinh(17/03/2023)
     * Edited by:
     * @param careRoom
     * @returns 
     */
    public async addCareRoom(careRoom) {
        await this.caroRooms.push(careRoom);
        // console.log(this.caroRooms.length);
    }

    /**
     * Description: function update information about caroRoom
     * Create by: BTLinh(17/03/2023)
     * Edited by:
     * @param careRoom
     * @returns 
     */
    public async updateCareRoom(careRoom) {
        this.caroRooms.map(room => {
            if (room.id === careRoom.id) {
                return {
                    ...room,
                    players: careRoom.players
                };
            }
            return room;
        });
    }

    /**
     * Description: function remove for careroom 
     * Create by: BTLinh(17/03/2023)
     * Edited by:
     * @param roomId
     * @returns 
     */
    public async removeCareRoom(roomId) {
        const index = this.caroRooms.findIndex(room => room.id === roomId);
        if (index !== -1) {
            this.caroRooms.splice(index, 1);
        }
    }

    public async sendPackageToUser(user: User, type: PACKAGE_HEADER, data: any) {
        const packageData = { type, data };
        const buffer = await encodeMessage(packageData, GameManager.filePath, 'caro.Response');
        user.getSocket().send(buffer);
    }


    public async updateRoomData(data: any) {
        const { roomId, result, otherPlayerResult } = data;
        const { userId, statusWin, timePlay } = result;

        // console.log(data);

        if (otherPlayerResult === undefined) {
            const roomData = await RedisMatching.hGet(roomId);
            if (userId === roomData.firstUserId) {
                console.log('data hset:', statusWin, timePlay)
                await RedisMatching.hSet(roomId, 'firstUserStatusWin', statusWin);
                await RedisMatching.hSet(roomId, 'firstUserTimePlay', timePlay);
            }
        }
        else {
            let userWin = '';
            if (otherPlayerResult) {
                if (statusWin === true && otherPlayerResult.statusWin === false) {
                    userWin = userId;
                } else if (otherPlayerResult.statusWin === true && statusWin === false) {
                    userWin = otherPlayerResult.userId;
                } else if (timePlay < otherPlayerResult.timePlay) {
                    userWin = userId;
                } else if (timePlay > otherPlayerResult.timePlay) {
                    userWin = otherPlayerResult.userId;
                } else {
                    userWin = otherPlayerResult.userId;
                }

                console.log('User Winner:', userWin);

                if (roomId) {
                    await RedisMatching.del(roomId);
                    await RedisMatching.del(`CM_${roomId}`);
                }
            }

        }


    }
}

export default GameManager;