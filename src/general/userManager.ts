import User from "./user";

class UserManager {
    users: any;

    constructor() {
        this.users = new Map<string, User>();
    }

    add(user: User) {
        this.users.set(user.getSocket(), user);
    }

    get(ws: any) {
        return this.users.get(ws);
    }

    delete(ws: any) {
        this.users.delete(ws);
    }


    addUser(user: User): void {
        this.users.set(user.getId(), user);
    }

    removeUser(user: User): void {
        this.users.delete(user.getId());
    }

    getUserById(userId: string): User | undefined {
        return this.users.get(userId);
    }

    getAllUsers(): User[] {
        return Array.from(this.users.values());
    }
}


export default UserManager;