import { v4 as uuidv4 } from 'uuid';
import User from './user';
import { encodeMessage } from '../utils/helpers';
import { ROOM_STATUS } from '../utils/constants';
import RedisClient from '../config/redis';
// import { OutgameService } from '../../server/services';
// import { ParamsEndUser } from '../../server/services/types/interface';

class Room {
    id: string; // room's id
    players = []; // users joinned room
    waitStartTime: any;
    roomStatus: ROOM_STATUS;
    roomGrId: string;
    mmr: number;

    constructor(id) {
        this.id = id;
        this.players = [];
        this.waitStartTime = Date.now();
        this.roomStatus = ROOM_STATUS.RS_WAITING;
    }

    static async create() {
        const id = Math.floor(Math.random() * 1000);
        const room = new Room(id);
        await RedisClient.set(`room${id}`, JSON.stringify(room));

        return room;
    }

    /**
     * Description: function get room by id in redis
     * Create by: BTLinh(17/03/2023)
     * Edited by:
     * @returns 
     */
    static async get(id) {
        const data = await RedisClient.get(`room${id}`);
        if (data) {
            return data;
        }
        return null;
    }

    /**
     * Description: function list room information 
     * Create by: BTLinh(17/03/2023)
     * Edited by:
     * @returns 
     */
    static async findAll() {
        const rooms = await RedisClient.scan();
        return rooms;
    }


    /**
     * Description: function add player for room
     * Create by: BTLinh(17/03/2023)
     * Edited by:
     * @param player
     * @returns 
     */
    async addPlayer(player) {
        this.players.push(player);
        await RedisClient.push(`room${this.id}`, JSON.stringify(this));
        return this.players;
    }

    // async join(player) {
    //     this.players.push(player);
    //     await RedisClient.push(`room${this.id}`, JSON.stringify(this));
    //     clearTimeout(this.waitStartTime);
    //     delete this.waitStartTime;
    //     return this.players;
    // }

    /**
     * Description: function remove player for room
     * Create by: BTLinh(17/03/2023)
     * Edited by:
     * @returns 
     */
    async removePlayer(player) {
        // Xóa người chơi ra khỏi danh sách các người chơi trong phòng
        this.players = this.players.filter(p => p !== player);

        await RedisClient.set(`room${this.id}`, JSON.stringify(this));
    }

    /**
     * Description: function remove room
     * Create by: BTLinh(17/03/2023)
     * Edited by:
     * @returns 
     */
    async removeRoom() {
        try {
            RedisClient.del(`room${this.id}`).then(() => {
                console.log('delete', this.id);
            }
            ).catch(err => console.log('error', err));
        } catch (error) {
            console.log(error);
        }

    }

    isPlaying() {
        return this.roomStatus === ROOM_STATUS.RS_PLAYING;
    }

    getId() {
        return this.id;
    }

    userLeave(user: User) {
        const index = this.players.indexOf(user);
        if (index > -1)
            this.players.splice(index, 1);
    }

    isUserEmpty() {
        return this.players.length = 0;
    }

    getMaster() {
        return this.players.length > 0 ? this.players[0] : null;
    }

    isMaster(user: User) {
        if (!this.getMaster())
            return false;
        return user.getId() == this.getMaster().getId();
    }

    getMember() {
        return this.players.length > 1 ? this.players[1] : null;
    }

    isMember(user: User) {
        for (let player of this.players) {
            if (user.getId() == player.getId()) {
                return true;
            }
        }
        return false;
    }


    joinRoom(user: User) {
        this.players.push(user);
        this.roomStatus = ROOM_STATUS.RS_PLAYING;
    }

    setRoomToPlaying() {
        this.roomStatus = ROOM_STATUS.RS_PLAYING;
    }

    getMemberCount() {
        return this.players.length;
    }

    sendDataAll(payload: any, userSkip: User = null) {
        this.players.forEach(player => {
            if (!userSkip || userSkip.getId() != player.getId())
                player.sendData(payload);
        })
    }

}


export default Room;




