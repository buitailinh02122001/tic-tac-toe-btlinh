import { MATCHING_MMR_RANGE } from './../utils/constants';
import User from "./user";


class RoomManager {
    rooms: any;

    constructor() {
        this.rooms = new Map();
    }

    add(room: any) {
        this.rooms.set(room.id, room);
    }

    get(id: string) {
        return this.rooms.get(id);
    }

    findExistRoom(msg) {
        let room = null;
        const { mmr } = msg;
        let indexMMR = MATCHING_MMR_RANGE.findIndex((ele) => (ele.from <= mmr && ele.to && ele.to >= mmr));
        if (indexMMR < 0) indexMMR = MATCHING_MMR_RANGE.length - 1;
        const rangeMMR = MATCHING_MMR_RANGE[indexMMR];
        for (const key of this.rooms.keys()) {
            const val = this.rooms.get(key);
            if ((rangeMMR.from <= val.mmr && rangeMMR.to && rangeMMR.to >= val.mmr) || (rangeMMR.from <= val.mmr && !rangeMMR.to)) {
                room = val;
                return room
            }
        };
        return room;
    }

    removeRoom(id: string) {
        this.rooms.delete(id);
    }
    isExistRoom(id: string) {
        return this.get(id) != null;
    }

    userLeaveRoom(user: User) {
        const room = user.getRoom();

        if (!room) return false;

        if (!this.isExistRoom(room.getId())) return false;

        room.userLeave(user);

        if (room.isUserEmpty())
            this.rooms.delete(room.getId());

        console.log("User leave room: " + user.getId);
        return true;

    }
}


export default RoomManager;